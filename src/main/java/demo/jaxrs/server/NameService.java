package demo.jaxrs.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Created by axu on 2/4/2016.
 */
public class NameService {

    @GET
    @Path("/info")
    @NameBoundTag
    public String info() {
        return "Info: using name binding";
    }

    @GET
    @Path("/info2")
    public String regularInfo() {
        return "Info: not using name binding";
    }
}
