package demo.jaxrs.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * Created by axu on 2/3/2016.
 */
@Path("/secured")
public class SecureService {

    @GET
    @SecuredBy("adminRole")
    @Path("/hello")
    public String hello() {
        return "Secured hello!";
    }


}
