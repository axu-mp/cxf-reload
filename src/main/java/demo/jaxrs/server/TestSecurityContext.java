package demo.jaxrs.server;

import org.apache.cxf.security.SecurityContext;

import java.security.Principal;

/**
 * Created by axu on 2/2/2016.
 * This is used for testing only.
 * It returns user "administrator" for the principal and permit any role to pass through.
 */
public class TestSecurityContext implements SecurityContext {

    @Override
    public Principal getUserPrincipal() {
        return new Principal() {
            @Override
            public String getName() {
                return "administrator";
            }
        };
    }

    @Override
    public boolean isUserInRole(String s) {
        System.out.println("in TestSecurityContext, role: "+s);

        return s.equalsIgnoreCase("adminRole");
    }

}
