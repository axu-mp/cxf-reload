package demo.jaxrs.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by axu on 1/26/2016.
 */
@Path("/orderservice/")
@Produces("application/json, application/xml")
public class OrderService {
    long orderId = 100;
    Map<Long, Order> orders = new HashMap<Long, Order>();

    public OrderService() {
        init();
    }

    @GET
    @Path("/orders/{id}")
    public Order getOrder(@PathParam("id") String id) {
        System.out.println("----invoking getOrder, Order id is: " + id);
        long idNumber = Long.parseLong(id);
        Order c = orders.get(idNumber);
        return c;
    }

    final void init() {

        Order o = new Order();
        o.setDescription("order 100");
        o.setId(100);

        orders.put(o.getId(), o);
    }
}