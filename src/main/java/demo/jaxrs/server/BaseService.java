package demo.jaxrs.server;

import org.apache.cxf.jaxrs.ext.MessageContext;

import javax.ws.rs.core.*;

/**
 * Created by axu on 1/29/2016.
 */
public class BaseService {
    @Context
    HttpHeaders headers;

    @Context
    private MessageContext mc;

    @Context
    private SecurityContext sc;

    public void printHeaders() {
        System.out.println("headers :");
        for(String header : headers.getRequestHeaders().keySet()){
            System.out.println(header+":"+headers.getHeaderString(header));
        }
        System.out.println("date:"+headers.getDate());
        System.out.println("length:"+headers.getLength());
        System.out.println("cookies:"+headers.getCookies());
    }

    public void printMessages() {
        System.out.println("message context");
        UriInfo uriInfo = mc.getUriInfo();
        System.out.println("uriInfo path:"+uriInfo.getPath());
        System.out.println("uriInfo base path:"+uriInfo.getBaseUri());
        System.out.println("uriInfo absolute path:"+uriInfo.getAbsolutePath());

        SecurityContext securityContext = mc.getSecurityContext();
        System.out.println("security context: principal: "+ (securityContext.getUserPrincipal() == null? null: securityContext.getUserPrincipal().getName()));

        Request request = mc.getRequest();
        System.out.println("request: method"+request.getMethod());

        HttpHeaders httpHeaders = mc.getHttpHeaders();
    }


}
