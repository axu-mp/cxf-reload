package demo.jaxrs.server;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by axu on 2/3/2016.
 * Custom annotation.
 *
 * To use:
 * @SecuredBy("role1")
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD })
public @interface SecuredBy {
    String[] value();
}
