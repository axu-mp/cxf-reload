package demo.jaxrs.server;

import org.apache.cxf.interceptor.security.SecureAnnotationsInterceptor;
import org.apache.cxf.jaxrs.interceptor.JAXRSInInterceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.security.SecurityContext;


/**
 * Created by axu on 2/2/2016.
 * This is used for testing inly.
 * Add this interceptor before the SecureAnnotationsInterceptor (for scanning annoation of @RolesAllowed) so we
 * can insert a fake security context to test.
 *
 * In the server.class, comment/uncomment this interceptor insertion point to test @RollesAllowed.
 * If comment out it, the REST call should throw Authroization exception because no proper securitycontext is present.
 */
public class TestSecurityContextInterceptor extends JAXRSInInterceptor {

    public TestSecurityContextInterceptor() {
        addBefore(SecureAnnotationsInterceptor.class.getName());
    }

    public void handleMessage(Message message) {
        // create
        message.put(SecurityContext.class, new TestSecurityContext());
    }
}
