package demo.jaxrs.server;

import javax.ws.rs.NameBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by axu on 2/3/2016.
 * This is JAX-RS name binding annotation.
 *
 * To use:
 * @NameBoundTag
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD })
public @interface NameBoundTag {
}
