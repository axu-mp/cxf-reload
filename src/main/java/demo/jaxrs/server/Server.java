/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package demo.jaxrs.server;

import org.apache.cxf.interceptor.security.SecureAnnotationsInterceptor;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.ResourceProvider;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class Server {

    static JAXRSServerFactoryBean systemWorkspaceFactoryBean = new JAXRSServerFactoryBean();
    static JAXRSServerFactoryBean devWorkspaceFactoryBean = new JAXRSServerFactoryBean();


    protected Server() throws Exception {

        initSystemWorkspace();
        initDevWorkspace();
    }

    public static void initSystemWorkspace() {
        // have to re-create this bean;
        systemWorkspaceFactoryBean = new JAXRSServerFactoryBean();

        List providers = new ArrayList();
        providers.add(new JacksonJsonProvider());
        providers.add(new NameBoundTagProvider());

        // use json provider if needed
        systemWorkspaceFactoryBean.setProviders(providers);

        //systemWorkspaceFactoryBean.

        systemWorkspaceFactoryBean.setResourceClasses(CustomerService.class);
        systemWorkspaceFactoryBean.setResourceClasses(OrderService.class);
        systemWorkspaceFactoryBean.setResourceClasses(SecureService.class);
        systemWorkspaceFactoryBean.setResourceClasses(NameService.class);

        // support JAXRS @RolesAllowed
        SecureAnnotationsInterceptor secureAnnotationsInterceptor = new SecureAnnotationsInterceptor();
        secureAnnotationsInterceptor.setSecuredObject(new CustomerService());

        // support custom annotation @SecuredBy
        SecureAnnotationsInterceptor secureAnnotationsInterceptor2 = new SecureAnnotationsInterceptor();
        secureAnnotationsInterceptor2.setAnnotationClassName(SecuredBy.class.getName());
        secureAnnotationsInterceptor2.setSecuredObject(new SecureService());

        List interceptors = new ArrayList();
        interceptors.add(secureAnnotationsInterceptor);
        interceptors.add(secureAnnotationsInterceptor2);

        // could add the secureAnnotationsInterceptor here too.
        //systemWorkspaceFactoryBean.setInInterceptors(interceptors);

        // can't use relative?
        systemWorkspaceFactoryBean.setAddress("http://localhost:9000/sys");

        org.apache.cxf.endpoint.Server sServer = systemWorkspaceFactoryBean.create();

        // comment the line below to see the Authorization exception when calling the service end point.
        // Adding the TestSecurityContextInterceptor will allow the service call to pass because of the fake securitycontext.
        sServer.getEndpoint().getInInterceptors().add(new TestSecurityContextInterceptor());

        // add SecureAnnotationInterceptor
        sServer.getEndpoint().getInInterceptors().addAll(interceptors);
    }

    public static void initDevWorkspace() {
        // have to re-create this bean;
        devWorkspaceFactoryBean = new JAXRSServerFactoryBean();
        // use json provider if needed
        devWorkspaceFactoryBean.setProvider(JacksonJsonProvider.class);

        devWorkspaceFactoryBean.setResourceClasses(CustomerService.class);
        devWorkspaceFactoryBean.setResourceProvider(CustomerService.class,
                new SingletonResourceProvider(new CustomerService()));
        devWorkspaceFactoryBean.setAddress("http://localhost:9000/dev");

        devWorkspaceFactoryBean.create();
    }

    // only works with the hard coded directory
    public static void reloadSystemWorkspace(String directoryName, String packageName) throws Exception {
        if( systemWorkspaceFactoryBean != null) {

            org.apache.cxf.endpoint.Server sServer = systemWorkspaceFactoryBean.getServer();
            sServer.stop();
            sServer.destroy();
        }

        List list =scanFolderForClass(directoryName, packageName);

        // set up user end point

        systemWorkspaceFactoryBean = new JAXRSServerFactoryBean();
        // use json provider if needed
        systemWorkspaceFactoryBean.setProvider(JacksonJsonProvider.class);

        List interceptors = buildInterceptors(list);
        systemWorkspaceFactoryBean.setInInterceptors(interceptors);

        systemWorkspaceFactoryBean.setResourceClasses(list);
        systemWorkspaceFactoryBean.setAddress("http://localhost:9000/sys");
        systemWorkspaceFactoryBean.create();

    }

    public static void reloadUserWorkspace(String directoryName, String packageName) throws Exception{
        if( devWorkspaceFactoryBean != null) {

            org.apache.cxf.endpoint.Server sServer = devWorkspaceFactoryBean.getServer();
            sServer.stop();
            sServer.destroy();
        }

        List list =scanFolderForClass(directoryName, packageName);

        // set up user end point
        devWorkspaceFactoryBean = new JAXRSServerFactoryBean();
        // use json provider if needed
        devWorkspaceFactoryBean.setProvider(JacksonJsonProvider.class);

        List interceptors = buildInterceptors(list);
        devWorkspaceFactoryBean.setInInterceptors(interceptors);

        devWorkspaceFactoryBean.setResourceClasses(list);
        devWorkspaceFactoryBean.setAddress("http://localhost:9000/dev");
        devWorkspaceFactoryBean.create();

    }

    private static List scanFolderForClass(String directoryName, String packageName) throws Exception {
        // Create a File object on the root of the directory containing the class file
        File directory = new File(directoryName);
        String fullPath = directoryName+"/"+packageName.replaceAll("\\.", "/")+"/";
        System.out.println("fullpath:"+fullPath);
        File dir = new File(fullPath);

        //convert the file to URL format
        URL url = directory.toURI().toURL();

        // Convert File to a URL
        URL[] urls = new URL[]{url};

        // Create a new class loader with the directory
        ClassLoader cl = new URLClassLoader(urls);

        File[] files = dir.listFiles();
        List list = new ArrayList();

        for(File filename: files) {
            if( filename.isFile()) {
                System.out.println("load class:" + filename.getName());
                Class claz = cl.loadClass(packageName+"."+filename.getName().replaceAll(".class", ""));
                list.add(claz);
            }
        }

        return list;
    }

    private static List buildInterceptors(List<Class> classList) throws Exception {
        List<SecureAnnotationsInterceptor> interceptors = new ArrayList<SecureAnnotationsInterceptor>();

        for( Class claz: classList) {
            Object object = claz.newInstance();
            SecureAnnotationsInterceptor secureAnnotationsInterceptor = new SecureAnnotationsInterceptor();
            secureAnnotationsInterceptor.setSecuredObject(object);

            interceptors.add(secureAnnotationsInterceptor);
        }
        return interceptors;
    }

    /**
     * test:
     * http://localhost:9000/sys/customerservice/customers/123
     * http://localhost:9000/dev/customerservice/customers/123
     *
     * test security annotation @RolesAllowed
     * http://localhost:9000/sys/customerservice/customers/123
     *
     * test security annotation @SecuredBy
     * http://localhost:9000/sys/secure/hello
     *
     * test reload
     * type "sys" at the console, then
     * http://localhost:9000/sys/bar
     *
     * type "user" at the console, then
     * http://localhost:9000/dev/bar
     *
     */
    public static void main(String args[]) throws Exception {
        new Server();
        System.out.println("Server ready...");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter quit to quit, user to reload user space, sys to reload system space:");

        String s = "";
        while (! s.contains("quit")) {
            s = br.readLine();

            if( s.contains("user") || s.contains("dev")){
                reloadUserWorkspace("c:/tmp/jersey/userspace/", "com.micropact.example");
            }else if (s.contains("sys")){
                reloadSystemWorkspace("c:/tmp/jersey/systemspace/", "com.micropact.example");
            } else {
                System.out.println("Wrong input");
            }
        }

        System.out.println("Server exiting");
        System.exit(0);
    }
}
